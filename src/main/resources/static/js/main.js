function addItemToOrder() {

    var name = $("#productInput").val();
    var quantity = $("#quantity").val();

    $('#orderTable').append('<tr><td>' + name + '</td><td>' + quantity + '</td><td><button class="btn btn-danger" style="background-color: red;" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);">X</button> </td></tr>');
}

function submitOrder() {

    console.log("submit");

    var data = [];
    $('#orderTable > tbody  > tr').each(function(){

        var item = $(this).children().eq(0).html();
        var quantity = $(this).children().eq(1).html();

        data.push({
            "itemName": item,
            "quantity" : quantity
        })
    });

    $.ajax({
        type: "POST",
        url: '/submit-order',
        data: JSON.stringify(data),
        contentType:"application/json; charset=utf-8",
        success: function(html) {
            alert(html);
            location.reload();
        }
    });

}