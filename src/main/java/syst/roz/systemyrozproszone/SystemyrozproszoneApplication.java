package syst.roz.systemyrozproszone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SystemyrozproszoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemyrozproszoneApplication.class, args);
	}
}
