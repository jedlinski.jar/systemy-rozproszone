package syst.roz.systemyrozproszone.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {

	Order findById(String id);
}
