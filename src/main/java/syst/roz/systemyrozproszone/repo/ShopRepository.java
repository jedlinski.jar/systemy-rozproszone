package syst.roz.systemyrozproszone.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.Shop;
import syst.roz.systemyrozproszone.model.User;

@Repository
public interface ShopRepository extends CrudRepository<User, Integer> {
}
