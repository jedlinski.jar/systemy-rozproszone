package syst.roz.systemyrozproszone.repo;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.Item;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CustomItemRepositoryImpl implements CustomItemRepository {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Item> nowez(String param) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Item.class);

		if (param.matches("\\d+")) {
			criteria.add(Restrictions.sqlRestriction(" id::text LIKE '%"+param+"%' "));
		}else {

			criteria.add(Restrictions.ilike("name", "%" + param + "%"));
		}


		return criteria.list();
	}
}
