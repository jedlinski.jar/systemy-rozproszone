package syst.roz.systemyrozproszone.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.Item;

import java.util.List;


public interface ItemRepository extends CrudRepository<Item, Integer> {

	Item findByName(String name);
}
