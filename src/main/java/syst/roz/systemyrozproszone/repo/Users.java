package syst.roz.systemyrozproszone.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import syst.roz.systemyrozproszone.model.User;

import java.util.List;

@Service
class Users implements UserDetailsManager {

	private UserRepository repo;

	@Autowired
	public Users(UserRepository repo) {this.repo = repo;}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = repo.findByUsername(username);
		if (user == null) {throw new UsernameNotFoundException("Username was not found. ");}
		List<GrantedAuthority> auth = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
		if (username.equals("admin")) {auth = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN");}
		String password = user.getPassword();
		return new org.springframework.security.core.userdetails.User(username, password, auth);
	}

	@Override
	public void createUser(UserDetails user) {
		repo.save((User) user);
	}

	@Override
	public void updateUser(UserDetails user) {
		repo.save((User) user);
	}

	@Override
	public void deleteUser(String username) {// TODO Auto-generated method stub
		User deluser = (User)this.loadUserByUsername(username);
		repo.delete(deluser);
	}

	@Override
	public void changePassword(String oldPassword, String newPassword) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean userExists(String username) {
		// TODO Auto-generated method stub
		return false;
	}

}