package syst.roz.systemyrozproszone.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

	User findByUsername(String username);
}
