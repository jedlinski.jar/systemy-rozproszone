INSERT INTO shops VALUES ('Brukowana 8, 57-120 Novigrad', 3);
INSERT INTO shops VALUES ('Tajemnicza 10, 12-345 Lipki', 4);

INSERT INTO users VALUES (3, 'user3', '5985cc91a8300de96813bdef25b57493e6c78e2da3b00788523553066f5943dd196025de88d4fc4b', 3);
insert into users VALUES (4, 'user4', '3ffb99b4871639d91840b927c14f422441a8a796a3244951b5bbf202ce05e097256437b792f54359', 4);

INSERT INTO items VALUES (111, 'Pralka Samsung', 1099.99);
INSERT INTO items VALUES (222, 'Pralka Indesit', 1499.99);
INSERT INTO items VALUES (333, 'Telewizor LG', 2099.99);
INSERT INTO items VALUES (444, 'Odtwarzacz MP3', 129.99);

insert into shop_stocks VALUES (111, 10, 9, 3);
insert into shop_stocks VALUES (222, 5, 10, 3);
insert into shop_stocks VALUES (333, 8, 11, 3);
insert into shop_stocks VALUES (444, 20, 12, 3);

insert into shop_stocks VALUES (111, 15, 13, 4);
insert into shop_stocks VALUES (222, 3, 14, 4);
insert into shop_stocks VALUES (333, 6, 15, 4);
insert into shop_stocks VALUES (444, 7, 16, 5);