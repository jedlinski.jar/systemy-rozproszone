package syst.roz.systemyrozproszone.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.Item;
import syst.roz.systemyrozproszone.model.Shop;
import syst.roz.systemyrozproszone.model.ShopStock;

import java.util.List;

@Repository
public interface ShopStockRepository extends CrudRepository<ShopStock, Integer>{

	List<ShopStock> findAllByItemId(int itemId);

	ShopStock findByShopAndAndItem(Shop shop, Item item);
}
