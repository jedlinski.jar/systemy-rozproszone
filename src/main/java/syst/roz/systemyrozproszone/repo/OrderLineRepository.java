package syst.roz.systemyrozproszone.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import syst.roz.systemyrozproszone.model.OrderLine;

@Repository
public interface OrderLineRepository extends CrudRepository<OrderLine, Integer>{
}
