--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.10
-- Dumped by pg_dump version 9.5.10

-- Started on 2017-12-07 22:50:41 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 231 (class 1259 OID 19814)
-- Name: items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE items (
  id integer NOT NULL,
  name text,
  price double precision
);


ALTER TABLE items OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 19890)
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE orders (
  id text NOT NULL,
  user_id integer,
  shop_id integer
);


ALTER TABLE orders OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 19908)
-- Name: orders_lines; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE orders_lines (
  id text NOT NULL,
  item_id integer,
  order_id text,
  quantity integer,
  price double precision
);


ALTER TABLE orders_lines OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 19859)
-- Name: shop_stocks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE shop_stocks (
  item_id integer,
  quantity integer,
  id integer NOT NULL,
  shop_id integer
);


ALTER TABLE shop_stocks OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 19832)
-- Name: shops; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE shops (
  address text,
  id integer NOT NULL
);


ALTER TABLE shops OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 19838)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
  id integer NOT NULL,
  username text,
  password text,
  shop_id integer
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 2433 (class 0 OID 19814)
-- Dependencies: 231
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY items (id, name, price) FROM stdin;
\.


--
-- TOC entry 2437 (class 0 OID 19890)
-- Dependencies: 235
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY orders (id, user_id, shop_id) FROM stdin;
\.


--
-- TOC entry 2438 (class 0 OID 19908)
-- Dependencies: 236
-- Data for Name: orders_lines; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY orders_lines (id, item_id, order_id, price) FROM stdin;
\.


--
-- TOC entry 2436 (class 0 OID 19859)
-- Dependencies: 234
-- Data for Name: shop_stocks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY shop_stocks (item_id, quantity, id, shop_id) FROM stdin;
\.


--
-- TOC entry 2434 (class 0 OID 19832)
-- Dependencies: 232
-- Data for Name: shops; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY shops (address, id) FROM stdin;
\.


--
-- TOC entry 2435 (class 0 OID 19838)
-- Dependencies: 233
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, username, password, shop_id) FROM stdin;
\.


--
-- TOC entry 2301 (class 2606 OID 19821)
-- Name: items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY items
  ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- TOC entry 2311 (class 2606 OID 19912)
-- Name: orders_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders_lines
  ADD CONSTRAINT orders_lines_pkey PRIMARY KEY (id);


--
-- TOC entry 2309 (class 2606 OID 19897)
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
  ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 2303 (class 2606 OID 19927)
-- Name: pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shops
  ADD CONSTRAINT pkey PRIMARY KEY (id);


--
-- TOC entry 2307 (class 2606 OID 19925)
-- Name: shop_stocks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shop_stocks
  ADD CONSTRAINT shop_stocks_pkey PRIMARY KEY (id);


--
-- TOC entry 2305 (class 2606 OID 19889)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
  ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2317 (class 2606 OID 19913)
-- Name: orders_lines_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders_lines
  ADD CONSTRAINT orders_lines_item_id_fkey FOREIGN KEY (item_id) REFERENCES items(id);


--
-- TOC entry 2318 (class 2606 OID 19918)
-- Name: orders_lines_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders_lines
  ADD CONSTRAINT orders_lines_order_id_fkey FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- TOC entry 2315 (class 2606 OID 19903)
-- Name: orders_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
  ADD CONSTRAINT orders_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2312 (class 2606 OID 19928)
-- Name: shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
  ADD CONSTRAINT shop_id_fkey FOREIGN KEY (shop_id) REFERENCES shops(id);


--
-- TOC entry 2316 (class 2606 OID 19933)
-- Name: shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orders
  ADD CONSTRAINT shop_id_fkey FOREIGN KEY (shop_id) REFERENCES shops(id);


--
-- TOC entry 2314 (class 2606 OID 19938)
-- Name: shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shop_stocks
  ADD CONSTRAINT shop_id_fkey FOREIGN KEY (shop_id) REFERENCES shops(id);


--
-- TOC entry 2313 (class 2606 OID 19870)
-- Name: shop_stocks_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shop_stocks
  ADD CONSTRAINT shop_stocks_item_id_fkey FOREIGN KEY (item_id) REFERENCES items(id);


-- Completed on 2017-12-07 22:50:41 CET

--
-- PostgreSQL database dump complete
--
