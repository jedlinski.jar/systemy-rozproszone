INSERT INTO shops VALUES ('Zmyślona 13, 57-120 Utopia', 1);
INSERT INTO shops VALUES ('Ukryta 6, 11-222 Rivia', 2);

INSERT INTO users VALUES (1, 'user1', '0d3e7578b8a7494d94f1342f469a3ece26f9ed4514b2dafc98bece1dfbab097aa445ef7ec403a905', 1);
insert into users VALUES (2, 'user2', 'd0dad0dc0f50ede9fa846ef564375a8d66dab417a6f98c9d57bf8c1b3e52bbc7f8f64f018a76f7ad', 2);

INSERT INTO items VALUES (111, 'Pralka Samsung', 1099.99);
INSERT INTO items VALUES (222, 'Pralka Indesit', 1499.99);
INSERT INTO items VALUES (333, 'Telewizor LG', 2099.99);
INSERT INTO items VALUES (444, 'Odtwarzacz MP3', 129.99);

insert into shop_stocks VALUES (111, 10, 1, 1);
insert into shop_stocks VALUES (222, 5, 2, 1);
insert into shop_stocks VALUES (333, 8, 3, 1);
insert into shop_stocks VALUES (444, 20, 4, 1);

insert into shop_stocks VALUES (111, 15, 5, 2);
insert into shop_stocks VALUES (222, 3, 6, 2);
insert into shop_stocks VALUES (333, 6, 7, 2);
insert into shop_stocks VALUES (444, 7, 8, 2);

