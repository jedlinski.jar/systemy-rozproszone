package syst.roz.systemyrozproszone.cont;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import syst.roz.systemyrozproszone.model.Shop;
import syst.roz.systemyrozproszone.repo.ShopRepository;

@Controller
@RequestMapping("/shop")
public class ShopController {

	@Autowired
	private ShopRepository shopRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@GetMapping
	private void putShop() {

		String user1 = passwordEncoder.encode("user3");
		String user4 = passwordEncoder.encode("user4");

		System.out.println("USERENC: " + user1);
		System.out.println("USERENC: " + user4);

	}
}
