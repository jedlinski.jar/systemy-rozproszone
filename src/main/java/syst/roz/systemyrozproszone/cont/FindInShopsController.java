package syst.roz.systemyrozproszone.cont;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import syst.roz.systemyrozproszone.model.ShopStock;
import syst.roz.systemyrozproszone.repo.ShopStockRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
@RequestMapping("/find-in-shops")
public class FindInShopsController {

	@Autowired
	private ShopStockRepository shopStockRepository;

	@GetMapping
	private String getView() {

		return "find-in-shops-view";
	}

	@PostMapping
	private String findInShopsAvailanility(@RequestParam int itemId, Model model) {

		List<ShopStock> allByItemId = shopStockRepository.findAllByItemId(itemId);

		List<ShopStock> stocks = allByItemId.stream()
				.filter(stock -> stock.getQuantity() > 0)
				.collect(toList());

		model.addAttribute("availableStocks", stocks);

		return "find-in-shops-view";
	}
}
