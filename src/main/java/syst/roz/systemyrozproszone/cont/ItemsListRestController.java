package syst.roz.systemyrozproszone.cont;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import syst.roz.systemyrozproszone.model.Item;
import syst.roz.systemyrozproszone.repo.CustomItemRepository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/find-item")
public class ItemsListRestController {

	@Autowired
	private CustomItemRepository itemRepository;

	@GetMapping
	private List<String> getItem(@RequestParam("term") String param) {

		return itemRepository.nowez(param).stream().map(Item::getName).collect(toList());
	}
}
