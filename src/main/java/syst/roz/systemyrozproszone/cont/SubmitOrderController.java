package syst.roz.systemyrozproszone.cont;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import syst.roz.systemyrozproszone.model.*;
import syst.roz.systemyrozproszone.repo.*;

import java.util.List;

@RestController
@RequestMapping("/submit-order")
public class SubmitOrderController {

	@Autowired
	private ItemRepository itemRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private ShopStockRepository shopStockRepository;
	@Autowired
	private OrderLineRepository orderLineRepository;


	@PostMapping(consumes = "application/json;charset=utf-8")
	private String submitOrder(@RequestBody List<SubmitOrderForm> form, Authentication authentication) {

		User user = userRepository.findByUsername(authentication.getName());
		Order order = new Order();

		form.forEach(row -> {
					Item item = itemRepository.findByName(row.getItemName());
					int quantity = row.getQuantity();


					ShopStock stockForCurrentShop = getStockForCurrentShop(user, item);
					int currentQuantity = stockForCurrentShop.getQuantity();

					if (currentQuantity < quantity) {
						throw new IllegalStateException("Niewystarczajaca liczba sztuk produktu [" + item.getName() + "]");
					}

					stockForCurrentShop.setQuantity(stockForCurrentShop.getQuantity() - row.getQuantity());

					shopStockRepository.save(stockForCurrentShop);

					OrderLine orderLine = new OrderLine();
					orderLine.setItem(item);
					orderLine.setOrder(order);
					orderLine.setPrice(item.getPrice());
					orderLine.setQuantity(row.getQuantity());

					order.getOrderLines().add(orderLine);
				}
		);

		double totalPrice = order.getOrderLines()
				.stream()
				.map(OrderLine::getPrice)
				.reduce(0d, (a, b) -> a + b);

		order.setUser(user);
		order.setShop(user.getShop());

		orderRepository.save(order);
		orderLineRepository.save(order.getOrderLines());

		return "Złożono zamówienie numer: " + order.getId() + "\nSuma: " + String.valueOf(totalPrice) + "zł.";
	}

	private ShopStock getStockForCurrentShop(User user, Item item) {
		return item.getShopStocks()
				.stream()
				.filter(shopStock -> shopStock.getShop().getId() == user.getShop().getId())
				.findFirst()
				.orElseThrow(() -> new IllegalStateException("Niepoprawny sklep"));
	}
}
