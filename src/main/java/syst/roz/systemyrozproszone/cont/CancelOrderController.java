package syst.roz.systemyrozproszone.cont;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import syst.roz.systemyrozproszone.model.Order;
import syst.roz.systemyrozproszone.model.OrderLine;
import syst.roz.systemyrozproszone.model.Shop;
import syst.roz.systemyrozproszone.model.ShopStock;
import syst.roz.systemyrozproszone.repo.OrderLineRepository;
import syst.roz.systemyrozproszone.repo.OrderRepository;
import syst.roz.systemyrozproszone.repo.ShopStockRepository;

import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/cancel-order")
public class CancelOrderController {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ShopStockRepository shopStockRepository;

	@Autowired
	private OrderLineRepository orderLineRepository;

	@GetMapping
	private String getView() {
		return "cancel-order-view";
	}

	@PostMapping
	private String cancelOrder(Model model, @RequestParam String orderId) {

		Order order = orderRepository.findById(orderId);

		Set<OrderLine> orderLines = order.getOrderLines();
		Shop shop = order.getShop();

		orderLines.forEach(line -> {
			ShopStock stock = shopStockRepository.findByShopAndAndItem(shop, line.getItem());

			stock.setQuantity(stock.getQuantity() + line.getQuantity());

			shopStockRepository.save(stock);

		});

		orderLineRepository.delete(orderLines);
		orderRepository.delete(order);

		model.addAttribute("message", "ok");

		return "cancel-order-view";
	}
}
