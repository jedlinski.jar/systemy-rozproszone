package syst.roz.systemyrozproszone.cont;

import java.io.Serializable;

public class SubmitOrderForm implements Serializable {

	public static final long serialVersionUID = 1L;

	private String itemName;
	private int quantity;

	public SubmitOrderForm() {
	}

	public SubmitOrderForm(String itemName, int quantity) {
		this.itemName = itemName;
		this.quantity = quantity;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
