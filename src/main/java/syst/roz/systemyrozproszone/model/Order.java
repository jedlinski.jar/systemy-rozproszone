package syst.roz.systemyrozproszone.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {

	@Id
	private String id;

	@ManyToOne
	@JoinColumn(name = "shop_id", nullable = false)
	private Shop shop;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@OneToMany(mappedBy = "order")
	private Set<OrderLine> orderLines;

	public Set<OrderLine> getOrderLines() {

		if (orderLines == null) {
			orderLines = new HashSet<>();
		}

		return orderLines;
	}

	public void setOrderLines(Set<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@PrePersist
	private void generateId() {

		this.id = System.currentTimeMillis() + "/" + shop.getId();

		int i = 0;

		for (OrderLine orderLine : orderLines) {

			orderLine.setId(this.id + "/" + i++);
		}
	}
}