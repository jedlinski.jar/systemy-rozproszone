package syst.roz.systemyrozproszone.model;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "shops")
public class Shop {

	@Id
	@GeneratedValue
	private int id;

	@Column
	private String address;

	@OneToMany(mappedBy = "shop")
	private Set<Order> orders;

	@OneToMany(mappedBy = "shop")
	private Set<ShopStock> shopStocks;

	@OneToMany(mappedBy = "shop")
	private Set<User> users;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
