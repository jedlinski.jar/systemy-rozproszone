package syst.roz.systemyrozproszone.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "items")
public class Item {

	@Id
	@GeneratedValue
	private int id;

	@Column
	private String name;

	@Column
	private double price;

	@OneToMany(mappedBy = "item")
	private Set<ShopStock> shopStocks;

	@OneToMany(mappedBy = "item")
	private Set<OrderLine> orderLines;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Set<ShopStock> getShopStocks() {
		return shopStocks;
	}

	public void setShopStocks(Set<ShopStock> shopStocks) {
		this.shopStocks = shopStocks;
	}

	public Set<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(Set<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
}
