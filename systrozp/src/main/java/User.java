import java.util.Arrays;
import java.util.Set;

public class User {

	private int id;

	private String username;

	private String password;

	private Set<Order> orders;

	private Shop shop;


	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
