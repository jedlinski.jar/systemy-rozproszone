import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class Order {

	private String id;

	private int shop;
	private int user;

	private Set<OrderLine> orderLines;

	public Order(ResultSet rs) {
		try {
			this.id = rs.getString("id");
			this.shop = rs.getInt("shop_id");
			this.user = rs.getInt("user_id");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Set<OrderLine> getOrderLines() {

		if (orderLines == null) {
			orderLines = new HashSet<>();
		}

		return orderLines;
	}

	public void setOrderLines(Set<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getShop() {
		return shop;
	}

	public int getUser() {
		return user;
	}
}