import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderLine {

	private String id;

	private int item;

	private String order;

	private int quantity;

	private double price;

	public OrderLine(ResultSet resultSet) {
		try {
			this.id = resultSet.getString("id");
			this.item = resultSet.getInt("item_id");
			this.order = resultSet.getString("order_id");
			this.quantity = resultSet.getInt("quantity");
			this.price = resultSet.getDouble("price");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getItem() {
		return item;
	}


	public String getOrder() {
		return order;
	}


	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
