
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SystemyRozproszoneApplication extends Application {

	private TableView<Order> table = new TableView<>();
	private DriverManagerDataSource dataSource;


	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException, SQLException {

		dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost:5432/central");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres");

		Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
		primaryStage.setTitle("Orders");
		Scene scene = new Scene(root, 500, 500);

		TableView<Order> ordersTable = createOrdersTable(scene);
		TableView<ShopStock> stocksTable = createStocksTable(scene);

		((TabPane) scene.getRoot()).getTabs().get(0).setContent(ordersTable);
		((TabPane) scene.getRoot()).getTabs().get(1).setContent(stocksTable);

		Timer timer = new Timer();
		TimerTask myTask = new TimerTask() {
			@Override
			public void run() {
					Platform.runLater(() -> {
						try {
							refreshTableData(ordersTable, stocksTable);
						} catch (SQLException e) {
							e.printStackTrace();
						}
					});
			}
		};

		timer.schedule(myTask, 0, 2000);

		refreshTableData(ordersTable, stocksTable);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void refreshTableData(TableView<Order> ordersTable, TableView<ShopStock> stocksTable) throws SQLException {

		Connection connection = dataSource.getConnection();
		ResultSet rs = connection.prepareStatement("select * from orders;").executeQuery();

		List<Order> orders = new ArrayList<>();

		while (rs.next()) {
			orders.add(new Order(rs));
		}

		ordersTable.setItems(FXCollections.observableArrayList(orders));

		rs = connection.prepareStatement("select * from shop_stocks;").executeQuery();

		List<ShopStock> stocks = new ArrayList<>();

		while (rs.next()) {
			stocks.add(new ShopStock(rs));
		}

		stocksTable.setItems(FXCollections.observableArrayList(stocks));

		connection.close();
	}

	private TableView<Order> createOrdersTable(Scene scene) {

		TableView<Order> table = new TableView<>();
		TableColumn<Order, String> firstNameCol = new TableColumn<>("Id");
		firstNameCol.setCellValueFactory(new PropertyValueFactory<>("id"));
		TableColumn<Order, String> lastNameCol = new TableColumn<>("Sklep");
		lastNameCol.setCellValueFactory(new PropertyValueFactory<>("shop"));
		TableColumn<Order, String> emailCol = new TableColumn<>("Użytkownik");
		emailCol.setCellValueFactory(new PropertyValueFactory<>("user"));

		table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);
		table.prefWidthProperty().bind(scene.widthProperty());
		table.prefHeightProperty().bind(scene.heightProperty());
		return table;
	}

	private TableView<ShopStock> createStocksTable(Scene scene) {

		TableView<ShopStock> tableView = new TableView<>();
		TableColumn<ShopStock, String> idCOl = new TableColumn<>("Id");
		idCOl.setCellValueFactory(new PropertyValueFactory<>("id"));

		TableColumn<ShopStock, String> shopCol = new TableColumn<>("Sklep");
		shopCol.setCellValueFactory(new PropertyValueFactory<>("shop"));

		TableColumn<ShopStock, String> itemCol = new TableColumn<>("Przedmiot");
		itemCol.setCellValueFactory(new PropertyValueFactory<>("item"));

		TableColumn<ShopStock, Integer> quantityCol = new TableColumn<>("Liczba sztuk");
		quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
		quantityCol.setCellFactory(column -> new TableCell<ShopStock, Integer>() {
			@Override
			protected void updateItem(Integer item, boolean empty) {
				super.updateItem(item, empty);

				if (item == null) {
					return;
				}

				setText(item.toString());

				TableRow currentRow = getTableRow();

				if (currentRow == null) {
					return;
				}

				if (item <= 3){
					currentRow.setStyle("-fx-background-color:red");
				}
			}
		});

		tableView.getColumns().addAll(idCOl, shopCol, itemCol, quantityCol);
		tableView.prefWidthProperty().bind(scene.widthProperty());
		tableView.prefHeightProperty().bind(scene.heightProperty());

		return tableView;
	}
}
