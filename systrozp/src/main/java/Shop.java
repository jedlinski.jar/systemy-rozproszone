import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public class Shop {

	private int id;

	private String address;

	private Set<Order> orders;
	private Set<ShopStock> shopStocks;
	private Set<User> users;

	public Shop(ResultSet resultSet) {
		try {
			this.id = resultSet.getInt("id");
			this.address = resultSet.getString("address");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
