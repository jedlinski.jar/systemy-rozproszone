import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public class Item {

	private int id;

	private String name;

	private double price;

	private Set<ShopStock> shopStocks;

	private Set<OrderLine> orderLines;

	public Item(ResultSet resultSet) {

		try {
			this.id = resultSet.getInt("id");
			this.name = resultSet.getString("name");
			this.price = resultSet.getDouble("price");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Set<ShopStock> getShopStocks() {
		return shopStocks;
	}

	public void setShopStocks(Set<ShopStock> shopStocks) {
		this.shopStocks = shopStocks;
	}

	public Set<OrderLine> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(Set<OrderLine> orderLines) {
		this.orderLines = orderLines;
	}
}
