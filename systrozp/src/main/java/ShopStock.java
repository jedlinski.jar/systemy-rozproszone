import java.sql.ResultSet;
import java.sql.SQLException;

public class ShopStock {

	private int id;

	private int shop;

	private int item;

	private int quantity;

	public ShopStock(ResultSet resultSet) {
		try {
			this.id = resultSet.getInt("id");
			this.shop = resultSet.getInt("shop_id");
			this.item = resultSet.getInt("item_id");
			this.quantity = resultSet.getInt("quantity");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getId() {
		return id;
	}

	public int getShop() {
		return shop;
	}


	public int getItem() {
		return item;
	}


	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
